const knex = require("../db/knex");

module.exports = {
  async getListadoFechas(fechaInicio = "2019-08-23", fechaFin = "2019-09-09") {
    return knex.raw(`
            select to_char(i::date, 'dd-MM-yyyy') as fecha from generate_series('${fechaInicio}', '${fechaFin}', '1 day'::interval) i
        `);
  },
  async getListadoRadioBases(offset = 0) {
    const query = knex
      .distinct("radiobase")
      .from("historico_trafico")
      .offset(offset)
      // .limit(10);
      .limit(250);


    return query;
  },
  async getHistoricoTrafico(
    fechaInicio = "2019-08-23",
    fechaFin = "2019-09-09",
    radiobases
  ) {
    const fields = [
      "radiobase",
      knex.raw(`to_char(fecha, 'dd-MM-YYYY') as fecha`),
      "trafico"
    ];
    const query = knex
      .select(fields)
      .from("historico_trafico")
      .whereIn("radiobase", radiobases)
      .andWhereBetween("fecha", [fechaInicio, fechaFin])
      .groupBy("radiobase", "fecha", "trafico")
      .orderBy("radiobase");

    return query;
  }
};
