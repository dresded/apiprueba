var express = require("express");
var bodyParser = require("body-parser");
var port = process.env.PORT || 8001;
var knex = require("./db/knex");
var functions = require("./src/functions");

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

let allowCrossDomain = function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
};
app.use(allowCrossDomain);

app.get("/historico_trafico", (req, res) => {
  let { fechaInicio, fechaFin, offset = 0 } = req.query;
  functions.getListadoRadioBases(offset).then(radiobases => {
    let listaDates = null
    let listaRadiobases = radiobases.reduce((accum, element) => {
      accum.push(element.radiobase);
      return accum;
    }, []);
    functions
      .getHistoricoTrafico(fechaInicio, fechaFin, listaRadiobases)
      .then(resultados => {
        functions.getListadoFechas(fechaInicio, fechaFin).then(fechas => {
          const listaFechas = fechas.rows;
          listaDates = listaFechas
          let listado = radiobases.reduce((accum, element) => {
            let listadoTrafico = listaFechas.reduce((accumTrafico, fecha) => {
                if (accumTrafico.length === 0) {
                    accumTrafico.push(element.radiobase);
                }
              let registroBuscado = resultados.find(
                res =>
                  res.radiobase === element.radiobase &&
                  res.fecha === fecha.fecha
              );

              if (registroBuscado) {
                let { trafico } = registroBuscado;
                accumTrafico.push(trafico);
              } else {
                accumTrafico.push(0);
              }
              return accumTrafico;
            }, []);
            accum.push(listadoTrafico);
            return accum;
          }, []);
          res.send({listado, listaDates });
        });
      });
  });
});

app.listen(port, function() {
  console.log("listening on port: ", port);
});
